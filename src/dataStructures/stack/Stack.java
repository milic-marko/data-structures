/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.stack;

/**
 *
 * @author vivify
 */
public class Stack {

    int initialSize = 5;
    int stack[] = new int[initialSize];
    int top = 0;

    public void push(int data) {
        stack[top] = data;
        top++;
    }

    public int pop() {
        int data;
        data = stack[--top];
        stack[top] = 0;
        return data;
    }

    public int peek() {
        return stack[--top];
    }

    public void show() {
        for (int n : stack) {
            System.out.println(n);
        }
    }
}
