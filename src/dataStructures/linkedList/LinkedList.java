/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.linkedList;

/**
 *
 * @author vivify
 */
public class LinkedList<T> {

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element) {
        Node<T> node = new Node<>();
        node.setValue(element);

        if (null != first) {
            first = node;
            last = node;
        } else {
            node.setNextElement(node);
            node.setPreviousElement(last);
            last = node;
        }
        size++;
    }

    public T get(int index) {
        checkElementIndex(index);
        return getElement(index).getValue();
    }
    
    private Node<T> getElement(int index) {
        
        if (index < (size >> 1)) {
            Node<T> node = first;
            for (int i = 0; i < index; i++)
                node = node.getNextElement();
            return node;
        } else {
            Node<T> node = last;
            for (int i = size - 1; i > index; i--)
                node = node.getPreviousElement();
            return node;
        }
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index)) {
            throw new IndexOutOfBoundsException();
        }
    }

    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    private void increaseSize() {
        size++;
    }
}
