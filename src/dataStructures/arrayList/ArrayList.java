/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.arrayList;

import java.util.Arrays;

/**
 *
 * @author vivify
 */
public class ArrayList<E> {

    private final int DEFAULT_SIZE = 10;
    private int size;
    private Object[] elements;

    public ArrayList() {
        elements = new Object[DEFAULT_SIZE];
    }

    public void add(E element) {
        if (size == elements.length) {
            increaseSize();
        }
        elements[size++] = element;

    }

    private void increaseSize() {
        int newSize = elements.length * 2;
        Arrays.copyOf(elements, newSize);
    }

    public E get(int index) {
        if (elements.length < index) {
            throw new IndexOutOfBoundsException("Index " + index + " is out of bounds");
        }
        return (E) elements[index];
    }

}
