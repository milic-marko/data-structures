/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.tree;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author vivify
 */
public class Node<T> {

    private T value;
    private Node<T> parent;
    private LinkedList<Node<T>> children = new LinkedList<>();

    public Node(Node<T> parent, T value) {
        this.parent = parent;
        this.value = value;
    }

    public List<Node<T>> getChildren() {
        return children;
    }

    public T getValue() {
        return value;
    }

    public boolean getHasParent() {
        return parent != null;
    }

    public boolean isFirstChild() {
        return parent != null && parent.children.peekFirst() == this;
    }

    public boolean isLastChild() {
        return parent != null && parent.children.peekLast() == this;
    }

    public Node<T> addChild(T child) {
        Node<T> temp = new Node(this, child);
        children.add(temp);
        return temp;
    }

}
