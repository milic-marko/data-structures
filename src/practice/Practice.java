/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practice;

import dataStructures.arrayList.ArrayList;
import dataStructures.binaryTree.BinaryTree;
import dataStructures.stack.Stack;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author vivify
 */
public class Practice {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //        ArrayList<String> arrayList= new ArrayList<>();
        //        arrayList.add("Test");
        //        System.out.println(arrayList.get(0));

//        BinaryTree binaryTree = new BinaryTree();
//        binaryTree.add(0);
//        binaryTree.add(1);
//        binaryTree.add(2);
//        binaryTree.add(3);
//        Stack stack = new Stack();
//        stack.push(2);
//        stack.push(4);
//        stack.push(17);
//        System.out.println(stack.pop());
//        System.out.println(stack.peek());
//        int[] array = {1, 3, 5, 7, 9};
//        binarySearch(array, 3);

        System.out.println(160 >> 3);
    }

    private static int binarySearch(int[] array, int item) {
        int low = 0;
        int high = array.length - 1;

        while (low <= high) {
            int mid = (low + high) / 2;
            int guess = array[mid];

            if (guess == item) {
                return guess;
            }

            if (guess > item) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }

        }
        return -1;
    }

    public static int bunnyEars(int bunnies) {
        if (bunnies == 0) {
            System.out.println(0 + "2");
            return 0;
        }
        int result = bunnyEars(bunnies - 1);
        return bunnyEars(bunnies - 1) + 2;
    }

    public static int fact(int n) {
        if (0 == n) {
            return 1;
        }
        return fact(n - 1) * n;
    }

}
